package task;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;

import java.io.File;

/**
 * Created by Admin on 11.07.2017.
 */
public class BuildFileValidator {
    /**
     * @param buildFile Build file
     * @param checker Validation type
     */
    public void validate(final BuildFileValidatorTask.BuildFile buildFile, final BuildfileChecker checker) {

        Project project = new Project();
        File file = new File(buildFile.getLocation());
        project.init();
        ProjectHelper.configureProject(project, file);
        checker.check(project);
    }
}
