package task;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

/**
 * Created by Admin on 11.07.2017.
 */
public class DefaultsChecker implements BuildfileChecker {
    @Override
    public void check(final Project project) {
        String defaultTarget = project.getDefaultTarget();
        if (!"main".equals(defaultTarget)) {
            throw new BuildException("Default target is missing.");
        }
    }
}
