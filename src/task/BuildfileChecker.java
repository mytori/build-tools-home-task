package task;
import org.apache.tools.ant.Project;

/**
 * Created by Admin on 11.07.2017.
 */
public interface BuildfileChecker {
    /**
     * @param project Build project
     * @return true if data is valid
     */
    void check(Project project);
}
