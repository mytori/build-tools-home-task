package task;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Admin on 11.07.2017.
 */
public class BuildFileValidatorTask extends Task {
    private boolean checkdepends = false;
    private boolean checkdefault = false;
    private boolean checknames = false;
    private BuildFile buildFile;
    private List<BuildFile> files = new ArrayList();
    /**
     *
     * @param file File for validation
     */
    public void setBuildFile(final BuildFile file) {
        buildFile = file;
    }

    /**
     *
     * @param checkdepends if true - checking depends method activated
     */
    public void setCheckdepends(final boolean checkdepends) {
        this.checkdepends = checkdepends;
    }

    /**
     * Checkdefault setter
     * @param checkdefault if true - checking default target method activated
     */
    public void setCheckdefault(final boolean checkdefault) {
        this.checkdefault = checkdefault;
    }
    /**
     * Checkdefault setter
     * @param checknames if true - checking targets names method activated
     */
    public void setChecknames(final boolean checknames) {
        this.checknames = checknames;
    }
    /**
     * Execute method
     */
    public void execute() {
        try {
            BuildFileValidator validator = new BuildFileValidator();
            for (Iterator it = files.iterator(); it.hasNext();) {      // 4
                BuildFile file = (BuildFile) it.next();

                if (checkdepends) {
                    validator.validate(file, new DependsChecker());
                }
                if (checknames) {
                    validator.validate(file, new NamesChecker());
                }
                if (checkdefault) {
                    validator.validate(file, new DefaultsChecker());
                }
            }
        } catch (BuildException e) {
            throw e;
        }
    }

    /** Factory method for creating nested 'BuildFiles's.
     * @return BuildFile
     * */
    public BuildFile createBuildFile() {
        BuildFile file = new BuildFile();
        files.add(file);
        return file;
    }

    /** A nested build file*/
    public class BuildFile {

        private String location;

        /** Bean constructor */
        public BuildFile() { }

        /** @param location File path */
        public void setLocation(final String location) {

            this.location = location;
        }
        /**
         * @return File location
         * */
        public String getLocation() {
            return location;
        }


    }
}
