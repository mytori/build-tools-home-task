package task;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Target;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;

/**
 * Created by Admin on 11.07.2017.
 */
public class DependsChecker implements BuildfileChecker {
    @Override
    public void check(final Project project) {
        Hashtable<String, Target> targets = project.getTargets();
        Set<String> keys = targets.keySet();
        for (String key: keys) {
            if (!"main".equals(key)) {
                Target target = targets.get(key);
                Enumeration<String> dependencies = target.getDependencies();
                if (dependencies.hasMoreElements()) {
                    throw new BuildException("Wrong dependencies");
                }
            }
        }
    }
}
