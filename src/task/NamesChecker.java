package task;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin on 11.07.2017.
 */
public class NamesChecker implements BuildfileChecker {
    private static final String TARGET_NAME_REG = "^(\\w?[a-z]{2,}(-\\w?[a-z]{2,})*)?$";
    @Override
    public void check(final Project project) {
        Set<String> targets = project.getTargets().keySet();
        Pattern pattern = Pattern.compile(TARGET_NAME_REG);
        for (String target: targets) {
            Matcher matcher = pattern.matcher(target);
            if (!matcher.matches()) {
                throw new BuildException("Targets wrong name.");
            }
        }
    }
}

