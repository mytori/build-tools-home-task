package project;

/**
 * Created by Admin on 11.07.2017.
 */
public class Subject {
    private String name;
    private Material material;
    private double volume;
    /**
     * @param name Subject name
     * @param material Subject material
     * @param volume Subject volume
     */
    public Subject(final String name, final Material material, final double volume) {
        super();
        this.name = name;
        this.material = material;
        this.volume = volume;
    }

    /**
     * Default constructor
     */
    public Subject() {
    }

    /**
     * Name getter
     * @return Subject name
     */
    public String getName() {
        return name;
    }

    /**
     * Name setter
     * @param name Set subject name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Material getter
     * @return Subject material
     */
    public Material getMaterial() {
        return material;
    }

    /**
     * Material setter
     * @param material Set subject material
     */
    public void setMaterial(final Material material) {
        this.material = material;
    }

    /**
     * Volume getter
     * @return Subject volume
     */
    public double getVolume() {
        return volume;
    }

    /**
     * Volume setter
     * @param volume Set volume
     */
    public void setVolume(final double volume) {
        this.volume = volume;
    }

    /**
     *
     * @return Subject mass
     */
    public double getMass() {
        return volume * material.getDensity();
    }

    @Override
    public String toString() {
        return name + ";" + material + ";" + volume + ";" + getMass();
    }
}
