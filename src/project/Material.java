package project;

/**
 * Created by Admin on 11.07.2017.
 */
public class Material {
    private final String name;
    private final double density;

    /**
     *
     * @param name Material name
     * @param density Material density
     */
    public Material(final String name, final double density) {
        super();
        this.name = name;
        this.density = density;
    }

    /**
     * Default constructor
     */
    public Material() {
        this(null, 0.0);
    }

    /**
     *
     * @return material name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return material density
     */
    public double getDensity() {
        return density;
    }

    @Override
    public String toString() {
        return name + ";" + density;
    }
}
