package project;

/**
 * Created by Admin on 11.07.2017.
 */
public final class Runner {
    private Runner() {
    }
    /**
     * Main method
     * @param args Main args
     */
    public static void main(final String[] args) {
        final Material steel = new Material("steel", 7850.0);
        final Material copper = new Material("copper", 8500.0);
        Subject wire = new Subject("wire", steel, 0.03);
        System.out.println(wire);
        wire.setMaterial(copper);
        System.out.print("The wire mass is " + wire.getMass() + " kg");
    }
}
